module.exports = {
  version: '2',
  // Find more about how to target a Specify repository at: https://specifyapp.com/developers/api#heading-parameters
  rules: [
    {
      name: 'Tokens compatible with Style Dictionary. SD will then transform these tokens to SCSS variables in SCSS files thanks to the SD config.json file.',
      parsers: [
        {
          name: 'filter',
          options: {
            query: {
              where: {
                collection: 'Tokens Studio',
                select: {
                  children: true
                }
              }
            },
            resolveAliases: true
          }
        },
        {
          name: 'to-style-dictionary',
          output: {
            type: 'directory',
            directoryPath: 'output/tokens/'
          }
        }
      ]
    },
    {
      name: 'Tokens as CSS variables in SCSS files',
      parsers: [
        {
          name: 'to-css-custom-properties',
          output: {
            type: 'file',
            filePath: 'output/tokens-css.scss'
          },
          options: {
            tokenNameTemplate: '--{{groups}}-{{token}}',
            selectorTemplate: '[data-theme=\"{{mode}}\"]',
          }
        }
      ]
    }
  ],
};